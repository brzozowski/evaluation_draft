package com.onespan.onespaneval.di.module

import com.onespan.onespaneval.data.mapper.Mapper
import com.onespan.onespaneval.data.network.EarthquakeApiRequest
import com.onespan.onespaneval.data.repository.EarthquakesDataRepository
import com.onespan.onespaneval.di.scope.ApplicationScope
import com.onespan.onespaneval.domain.repository.EarthquakesRepository
import dagger.Module
import dagger.Provides

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module
object DataModule {

    @ApplicationScope
    @Provides
    @JvmStatic
    fun provideMapper(): Mapper = Mapper()

    @Provides
    @ApplicationScope
    @JvmStatic
    internal fun providesEarthquakesRepository(
            earthquakeApiService: EarthquakeApiRequest,
            mapper: Mapper
    ): EarthquakesRepository =
            EarthquakesDataRepository(earthquakeApiService, mapper)
}