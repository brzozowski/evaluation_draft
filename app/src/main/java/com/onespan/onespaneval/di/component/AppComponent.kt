package com.onespan.onespaneval.di.component

import com.onespan.onespaneval.App
import com.onespan.onespaneval.di.module.ActivityBuilder
import com.onespan.onespaneval.di.module.AppModule
import com.onespan.onespaneval.di.module.DataModule
import com.onespan.onespaneval.di.module.NetworkModule
import com.onespan.onespaneval.di.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@ApplicationScope
@Component(
    modules = [
        (AppModule::class),
        (AndroidSupportInjectionModule::class),
        (ActivityBuilder::class),
        (NetworkModule::class),
        (DataModule::class)
    ]
)
@SuppressWarnings("unchecked")
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder
        fun build(): AppComponent
    }
}