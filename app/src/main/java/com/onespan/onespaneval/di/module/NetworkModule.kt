package com.onespan.onespaneval.di.module

import com.onespan.onespaneval.BuildConfig
import com.onespan.onespaneval.data.network.EarthquakeApiRequest
import com.onespan.onespaneval.data.network.Constants
import com.onespan.onespaneval.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module
object NetworkModule {

    @ApplicationScope
    @Provides
    @JvmStatic
    fun apiEndpoint() = Constants.Api.DATA_URL

    @ApplicationScope
    @Provides
    @JvmStatic
    fun provideCompositeDisposable() = CompositeDisposable()

    @ApplicationScope
    @Provides
    @JvmStatic
    fun provideOkHttpBuilder(): OkHttpClient.Builder {
        val okHttpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BASIC
            okHttpBuilder.addInterceptor(logging)
        }
        return okHttpBuilder.apply {
            readTimeout(15.toLong(), TimeUnit.SECONDS)
            connectTimeout(15.toLong(), TimeUnit.SECONDS)
        }
    }

    @ApplicationScope
    @Provides
    @JvmStatic
    fun provideRetrofit(retrofitBuilder: Retrofit.Builder,
                            okHttpClientBuilder: OkHttpClient.Builder,
                            baseUrl: String): Retrofit {
        val client = okHttpClientBuilder.build()
        return retrofitBuilder
                .client(client)
                .baseUrl(baseUrl)
                .build()
    }

    @ApplicationScope
    @Provides
    @JvmStatic
    fun provideRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @ApplicationScope
    @Provides
    @JvmStatic
    fun provideApiService(retrofit: Retrofit): EarthquakeApiRequest {
        return retrofit.create(EarthquakeApiRequest::class.java)
    }
}