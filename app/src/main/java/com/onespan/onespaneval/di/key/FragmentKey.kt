package com.onespan.onespaneval.di.key

import androidx.fragment.app.Fragment
import dagger.MapKey
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.reflect.KClass

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER)
@Retention(value = RUNTIME)
@MapKey
internal annotation class FragmentKey(val value: KClass<out Fragment>)