package com.onespan.onespaneval.di.module

import android.app.Application
import android.content.Context
import com.onespan.onespaneval.App
import com.onespan.onespaneval.di.qualifier.ApplicationContext
import com.onespan.onespaneval.di.scope.ApplicationScope
import dagger.Binds
import dagger.Module

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module(
    includes = [
        (ActivityBuilder::class)
])
@SuppressWarnings("unchecked")
abstract class AppModule {

    @Binds
    abstract fun bindApplication(app: App): Application

    @Binds
    @ApplicationScope
    @ApplicationContext
    abstract fun bindApplicationContext(application: Application): Context
}