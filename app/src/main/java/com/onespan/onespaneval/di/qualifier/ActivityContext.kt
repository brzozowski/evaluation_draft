package com.onespan.onespaneval.di.qualifier

import javax.inject.Qualifier

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityContext