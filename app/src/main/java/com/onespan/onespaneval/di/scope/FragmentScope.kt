package com.onespan.onespaneval.di.scope

import javax.inject.Scope

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope