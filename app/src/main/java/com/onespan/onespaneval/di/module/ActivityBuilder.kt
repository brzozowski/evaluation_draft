package com.onespan.onespaneval.di.module

import com.onespan.onespaneval.ui.main.MainActivity
import com.onespan.onespaneval.di.scope.ActivityScope
import com.onespan.onespaneval.ui.main.di.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivityInjector(): MainActivity
}