package com.onespan.onespaneval.domain.usecase

import com.onespan.onespaneval.data.model.Earthquake
import io.reactivex.Single

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

interface EarthquakeUseCase {
    fun getEarthquakes(): Single<List<Earthquake>>
}