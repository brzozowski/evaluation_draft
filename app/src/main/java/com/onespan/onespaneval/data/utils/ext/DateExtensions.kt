package com.onespan.onespaneval.data.utils.ext

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

fun Long.formatDate(): String {
    return try {
        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val date = Date(this)
        sdf.format(date)
    } catch (e: Exception) {
        e.toString()
    }
}
