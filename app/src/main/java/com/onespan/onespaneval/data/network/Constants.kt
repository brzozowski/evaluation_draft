package com.onespan.onespaneval.data.network

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

object Constants {

    interface Api {
        companion object {
            const val DATA_URL = "https://earthquake.usgs.gov"
        }
    }
}