package com.onespan.onespaneval.data.model

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

data class Metadata(
        val generated: Long,
        val url: String,
        val title: String,
        val status: Int,
        val api: String,
        val limit: Int,
        val offset: Int,
        val count: Int
)