package com.onespan.onespaneval.data.model

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

data class Feature(
        val type: String,
        val properties: Properties,
        val geometry: Geometry,
        val id: String
)