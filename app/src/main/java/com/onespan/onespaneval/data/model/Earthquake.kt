package com.onespan.onespaneval.data.model

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

data class Earthquake(
    val place: String,
    val time: Long,
    val mag: Double)