package com.onespan.onespaneval.data.model

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

data class Properties(
        val mag: Double,
        val place: String,
        val time: Long,
        val updated: Long,
        val tz: Int,
        val url: String,
        val detail: String,
        val felt: Int,
        val cdi: Double,
        val mmi: String,
        val alert: String,
        val status: String,
        val tsunami: Int,
        val sig: Int,
        val net: String,
        val code: Int,
        val ids: String,
        val sources: String,
        val types: String,
        val nst: Int,
        val dmin: Double,
        val rms: Double,
        val gap: Int,
        val magType: String,
        val type: String,
        val title: String
)