package com.onespan.onespaneval.data.network

import com.onespan.onespaneval.data.model.FeatureCollection
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

interface EarthquakeApiRequest {

    @GET("/fdsnws/event/{page}/query?format=geojson")
    fun getEarthquakes(
            @Path("page") page: Int? = 1,
            @Query("minmagnitude") minMagnitude: Double? = 2.5,
            @Query("limit") limit: Int? = 20,
            @Query("maxradiuskm") maxradiuskm: Int? = 100,
            @Query("longitude") longitude: Double? = -122.431297,
            @Query("latitude") latitude: Double? = 37.773972,
            @Query("starttime") starttime: String? = "2018-10-22",
            @Query("endtime") endtime: String? = "2019-01-02"
            ): Single<FeatureCollection>
}