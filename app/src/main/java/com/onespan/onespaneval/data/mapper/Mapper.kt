package com.onespan.onespaneval.data.mapper

import com.onespan.onespaneval.data.model.Earthquake
import com.onespan.onespaneval.data.model.Feature

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

class Mapper {

    fun translate(earthquakes: List<Feature>): List<Earthquake> {
        return earthquakes
                .map {
                    translate(it)
                }
                .asSequence()
                .toList()
    }

    val translate: (Feature) -> Earthquake = { feature ->
        Earthquake(
                feature.properties.place,
                feature.properties.time,
                feature.properties.mag
        )
    }
}