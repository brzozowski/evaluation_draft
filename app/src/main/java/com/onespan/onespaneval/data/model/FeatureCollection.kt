package com.onespan.onespaneval.data.model

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

data class FeatureCollection(
       val type: String,
       val metadata: Metadata,
       val features: List<Feature>,
       val bbox: List<Double>
)