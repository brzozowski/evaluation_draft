package com.onespan.onespaneval.data.model

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

data class Geometry(
        val type: String,
        val coordinates: List<Double>
)