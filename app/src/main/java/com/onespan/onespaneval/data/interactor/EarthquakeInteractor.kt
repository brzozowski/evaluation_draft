package com.onespan.onespaneval.data.interactor

import com.onespan.onespaneval.data.model.Earthquake
import com.onespan.onespaneval.domain.repository.EarthquakesRepository
import com.onespan.onespaneval.domain.usecase.EarthquakeUseCase
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

class EarthquakeInteractor @Inject constructor(
        private val earthquakesDataRepository: EarthquakesRepository
) : EarthquakeUseCase {
    override fun getEarthquakes(): Single<List<Earthquake>> = earthquakesDataRepository.getEarthquakes()
}