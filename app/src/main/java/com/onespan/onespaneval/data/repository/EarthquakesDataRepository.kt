package com.onespan.onespaneval.data.repository

import com.onespan.onespaneval.data.mapper.Mapper
import com.onespan.onespaneval.data.model.Earthquake
import com.onespan.onespaneval.data.network.EarthquakeApiRequest
import com.onespan.onespaneval.domain.repository.EarthquakesRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

class EarthquakesDataRepository @Inject constructor(
        private val earthquakeApiService: EarthquakeApiRequest,
        private val mapper: Mapper) : EarthquakesRepository {

    override fun getEarthquakes(): Single<List<Earthquake>> =
        earthquakeApiService.getEarthquakes()
                .subscribeOn(Schedulers.io())
                .map { earthquakes ->
                    Single.just(mapper.translate(earthquakes.features))
                }
                .flatMap { earthquakes -> earthquakes }

}