package com.onespan.onespaneval.ui.main.di

import androidx.appcompat.app.AppCompatActivity
import com.onespan.onespaneval.di.scope.ActivityScope
import com.onespan.onespaneval.ui.base.di.BaseActivityModule
import com.onespan.onespaneval.ui.main.MainActivity
import dagger.Binds
import dagger.Module

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module(includes = [BaseActivityModule::class, MainFragmentBuilder::class])
abstract class MainActivityModule {

    @ActivityScope
    @Binds
    abstract fun bindMainActivity(mainActivity: MainActivity): AppCompatActivity
}