package com.onespan.onespaneval.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.onespan.onespaneval.di.scope.FragmentScope
import com.onespan.onespaneval.ui.base.mvp.MvpPresenter
import com.onespan.onespaneval.ui.base.mvp.MvpView
import dagger.android.support.AndroidSupportInjection

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@FragmentScope
abstract class BaseFragment<V : MvpView, P : MvpPresenter<V>> : Fragment(), MvpView {

    protected abstract val presenter: P
    protected abstract val mvpView: V

    protected open val layoutId: Int? = null

    private var unbinder: Unbinder? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        layoutId?.let {
            return inflater.inflate(it, container, false)
        } ?: presenter.initialize(mvpView)

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)

        layoutId?.let {
            presenter.initialize(mvpView)
        }
    }

    override fun onDestroyView() {
        presenter.terminate()
        super.onDestroyView()
        unbinder?.unbind()
    }
}