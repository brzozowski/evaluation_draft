package com.onespan.onespaneval.ui.base

import com.onespan.onespaneval.ui.base.mvp.MvpPresenter
import com.onespan.onespaneval.ui.base.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    protected var view: V? = null

    override fun initialize(view: V) {
        this.view = view
    }

    protected fun disposable(disposableProvider: () -> Disposable) {
        checkNotNull(compositeDisposable) { "CompositeDisposable wasn't set in presenter constructor" }
        compositeDisposable += disposableProvider()
    }

    override fun terminate() {
        view = null
        compositeDisposable?.clear()
    }
}