package com.onespan.onespaneval.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.onespan.onespaneval.R
import com.onespan.onespaneval.data.model.Earthquake
import com.onespan.onespaneval.data.utils.ext.formatDate

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

class EarthquakesListAdapter(private val list: List<Earthquake>)
    : RecyclerView.Adapter<EarthquakeListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EarthquakeListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EarthquakeListViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: EarthquakeListViewHolder, position: Int) {
        val earthquake: Earthquake = list[position]
        holder.bind(earthquake)
    }

    override fun getItemCount(): Int = list.size
}

class EarthquakeListViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_earthquake, parent, false)) {

    private var mPlaceView: TextView? = null
    private var mTimeView: TextView? = null
    private var mMagView: TextView? = null

    init {
        mPlaceView = itemView.findViewById(R.id.location)
        mTimeView = itemView.findViewById(R.id.time)
        mMagView = itemView.findViewById(R.id.magnitude)
    }

    fun bind(earthquake: Earthquake) {
        mPlaceView?.text = earthquake.place
        mTimeView?.text = earthquake.time.formatDate()
        mMagView?.text = earthquake.mag.toString()
    }
}