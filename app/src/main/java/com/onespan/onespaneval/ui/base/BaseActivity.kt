package com.onespan.onespaneval.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import butterknife.ButterKnife
import butterknife.Unbinder
import com.onespan.onespaneval.R
import com.onespan.onespaneval.di.scope.ActivityScope
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@ActivityScope
abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    private var unbinder: Unbinder? = null

    protected abstract val initialFragment: Fragment?

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_base)
        unbinder = ButterKnife.bind(this)

        if (savedInstanceState == null) {
            initialFragment?.let {
                replaceContent(it)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder?.unbind()
    }

    private fun replaceContent(fragment: Fragment) {
        supportFragmentManager.commit {
            replace(R.id.baseActivityContent, fragment)
        }
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}