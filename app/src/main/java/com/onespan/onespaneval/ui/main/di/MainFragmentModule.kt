package com.onespan.onespaneval.ui.main.di

import androidx.fragment.app.Fragment
import com.onespan.onespaneval.data.interactor.EarthquakeInteractor
import com.onespan.onespaneval.di.key.FragmentKey
import com.onespan.onespaneval.di.scope.FragmentScope
import com.onespan.onespaneval.domain.repository.EarthquakesRepository
import com.onespan.onespaneval.domain.usecase.EarthquakeUseCase
import com.onespan.onespaneval.ui.main.MainFragment
import com.onespan.onespaneval.ui.main.MainPresenter
import com.onespan.onespaneval.ui.main.mvp.MainContract
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module
abstract class MainFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(MainFragment::class)
    abstract fun bindMainFragment(mainFragment: MainFragment): Fragment

    @FragmentScope
    @Binds
    internal abstract fun mainPresenter(presenter: MainPresenter): MainContract.Presenter

    @Module
    companion object {

        @Provides
        @JvmStatic
        internal fun provideMainUseCase(earthquakesRepository: EarthquakesRepository): EarthquakeUseCase =
                EarthquakeInteractor(earthquakesRepository)
    }
}