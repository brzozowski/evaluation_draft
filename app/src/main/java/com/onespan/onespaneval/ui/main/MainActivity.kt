package com.onespan.onespaneval.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.onespan.onespaneval.R
import com.onespan.onespaneval.ui.base.BaseActivity
import javax.inject.Inject

class MainActivity @Inject constructor() : BaseActivity() {

    override val initialFragment: Fragment?
        get() = MainFragment.newInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}