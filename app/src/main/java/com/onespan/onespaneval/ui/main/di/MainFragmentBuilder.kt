package com.onespan.onespaneval.ui.main.di

import com.onespan.onespaneval.di.scope.FragmentScope
import com.onespan.onespaneval.ui.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module
abstract class MainFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    abstract fun bindMainFragment(): MainFragment
}