package com.onespan.onespaneval.ui.main

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.OnClick
import com.onespan.onespaneval.R
import com.onespan.onespaneval.data.model.Earthquake
import com.onespan.onespaneval.ui.base.BaseFragment
import com.onespan.onespaneval.ui.main.adapter.EarthquakesListAdapter
import com.onespan.onespaneval.ui.main.mvp.MainContract
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

class MainFragment @Inject constructor(): BaseFragment<MainContract.View, MainContract.Presenter>(), MainContract.View {

    private lateinit var progressDialog: ProgressDialog

    @Inject
    override lateinit var presenter: MainContract.Presenter

    override val mvpView: MainContract.View = this
    override val layoutId: Int? = R.layout.fragment_main

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = ProgressDialog(context, R.style.AppTheme_Dark_Dialog)
    }

    override fun showProgress() {
        progressDialog.setMessage(getString(R.string.loading_data_progress_info))
        progressDialog.show()
    }

    override fun hideProgress() {
        progressDialog.dismiss()
    }

    override fun showLoadingEarthquakesError() {
        Toast.makeText(activity?.applicationContext, R.string.fetch_data_error, Toast.LENGTH_LONG).show()
    }

    @OnClick(R.id.button_id)
    fun onRetrievedBtnPressed() {
        presenter.loadData()
    }

    override fun setEarthquakes(items: List<Earthquake>) {
        earthquake_recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = EarthquakesListAdapter(items)
        }
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}