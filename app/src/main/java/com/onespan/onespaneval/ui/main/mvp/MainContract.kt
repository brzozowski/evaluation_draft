package com.onespan.onespaneval.ui.main.mvp

import com.onespan.onespaneval.data.model.Earthquake
import com.onespan.onespaneval.data.model.FeatureCollection
import com.onespan.onespaneval.ui.base.mvp.MvpPresenter
import com.onespan.onespaneval.ui.base.mvp.MvpView

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

interface MainContract {

    interface View : MvpView {
        fun showProgress()
        fun hideProgress()
        fun showLoadingEarthquakesError()
        fun setEarthquakes(items: List<Earthquake>)
    }

    interface Presenter : MvpPresenter<View> {
        fun loadData()
    }
}