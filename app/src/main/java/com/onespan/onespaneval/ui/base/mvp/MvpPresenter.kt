package com.onespan.onespaneval.ui.base.mvp

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

interface MvpPresenter<V : MvpView> {
    fun initialize(view: V)
    fun terminate()
}
