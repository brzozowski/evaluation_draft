package com.onespan.onespaneval.ui.base.di

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.onespan.onespaneval.di.qualifier.ActivityContext
import com.onespan.onespaneval.di.scope.ActivityScope
import dagger.Binds
import dagger.Module

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

@Module
abstract class BaseActivityModule {

    @Binds
    @ActivityScope
    @ActivityContext
    abstract fun bindActivityContext(activity: AppCompatActivity): Context
}