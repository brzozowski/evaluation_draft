package com.onespan.onespaneval.ui.main

import com.onespan.onespaneval.domain.usecase.EarthquakeUseCase
import com.onespan.onespaneval.ui.base.BasePresenter
import com.onespan.onespaneval.ui.main.mvp.MainContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by MARCIN BRZOZOWSKI on 08 October, 2019
 */

internal class MainPresenter @Inject constructor(
        private val earthquakeUseCase: EarthquakeUseCase
) : BasePresenter<MainContract.View>(), MainContract.Presenter {

    // I bet internet connection is established
    override fun loadData() {

        disposable {
            earthquakeUseCase.getEarthquakes()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        view?.showProgress()
                    }
                    .subscribe({ data ->
                        view?.hideProgress()
                        view?.setEarthquakes(data)
                    }, { error ->
                        Timber.e(error, "Loading earthquakes failed")
                        view?.hideProgress()
                        view?.showLoadingEarthquakesError()
                    })
        }
    }
}